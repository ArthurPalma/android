package com.example.minijeucombat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Inscription extends AppCompatActivity {

    EditText nom, prenom, email, login, mdp;
    Button valider;
    Spinner age, sexe;

    DatabaseReference databasePlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        databasePlayer = FirebaseDatabase.getInstance().getReference("player");

        nom = findViewById(R.id.nom);
        prenom = findViewById(R.id.prenom);
        email = findViewById(R.id.mail);
        login = findViewById(R.id.login);
        mdp = findViewById(R.id.mdp);
        valider = findViewById(R.id.valider);
        age = findViewById(R.id.age);
        sexe = findViewById(R.id.sexe);

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPlayer();
            }
        });
    }

    private void addPlayer(){
        String playername = nom.getText().toString().trim();
        String playersurname = prenom.getText().toString().trim();
        String playeremail = email.getText().toString().trim();
        String playerlogin = login.getText().toString().trim();
        String playerpwd = mdp.getText().toString().trim();
        Integer playerage = Integer.parseInt(age.getSelectedItem().toString());
        String playersexe = sexe.getSelectedItem().toString();

        if (!TextUtils.isEmpty(playername) && !TextUtils.isEmpty(playersurname ) && !TextUtils.isEmpty(playeremail) && !TextUtils.isEmpty(playerlogin) && !TextUtils.isEmpty(playerpwd)){

            String id = databasePlayer.push().getKey();

            Player player = new Player (id, playername, playersurname, playerage, playersexe, playeremail, playerpwd, playerlogin);

            databasePlayer.child(id).setValue(player);

            Toast.makeText(this, "Player added", Toast.LENGTH_LONG).show();

            nom.getText().clear();
            prenom.getText().clear();
            email.getText().clear();
            login.getText().clear();
            mdp.getText().clear();
            age.setSelection(0);
            sexe.setSelection(0);

        }else{
            Toast.makeText(this, "You have to fill all the data", Toast.LENGTH_LONG).show();
        }
    }

    public void Go_Remerciement(View v) {
        Intent intent=new Intent(this,Remerciement.class);
        startActivity(intent);
    }

    public void fermer(View v) {

        finish() ;
    }
}
