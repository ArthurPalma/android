package com.example.minijeucombat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }
    public void Go_Regles(View v) {
        Intent intent=new Intent(this,Regles.class);
        startActivity(intent);
    }

    public void Go_Jeux(View v) {
        Intent intent=new Intent(this,Jeux.class);
        startActivity(intent);
    }

    public void Go_Classement(View v) {
        Intent intent=new Intent(this,Classement.class);
        startActivity(intent);
    }

    public void Go_Main(View v) {
        Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}
