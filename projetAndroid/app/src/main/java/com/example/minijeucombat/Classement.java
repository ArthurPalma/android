package com.example.minijeucombat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Classement extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classement);
    }

    public void Go_Menu(View v) {
        Intent intent=new Intent(this,Menu.class);
        startActivity(intent);
    }
}
