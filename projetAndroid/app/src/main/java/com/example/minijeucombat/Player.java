package com.example.minijeucombat;

public class Player {

    String playerId;
    String playerName;
    String playerSurname;
    Integer playerAge;
    String playerSex;
    String playerEmail;
    String playerPwd;
    String playerLogin;
    Integer playerScore;

    public Player(){

    }


    public Player(String playerId, String playerName, String playerSurname, Integer playerAge, String playerSex, String playerEmail, String playerPwd, String playerLogin) {
        this.playerId = playerId;
        this.playerName = playerName;
        this.playerSurname = playerSurname;
        this.playerAge = playerAge;
        this.playerSex = playerSex;
        this.playerEmail = playerEmail;
        this.playerPwd = playerPwd;
        this.playerLogin = playerLogin;
        this.playerScore = 0;
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getPlayerSurname() {
        return playerSurname;
    }

    public Integer getPlayerAge() {
        return playerAge;
    }

    public String getPlayerSex() {
        return playerSex;
    }

    public String getPlayerEmail() {
        return playerEmail;
    }

    public Integer getPlayerScore() {
        return playerScore;
    }

    public String getPlayerPwd() {
        return playerPwd;
    }

    public String getPlayerLogin() {
        return playerLogin;
    }
}
